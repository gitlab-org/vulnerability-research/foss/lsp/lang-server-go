package main

import (
	"os"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lsp/lang-server-go/clicmds"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func main() {
	app := cli.NewApp()
	app.Name = "lang-server-go"
	app.Usage = "A generic go language server"

	app.Commands = []*cli.Command{
		{
			Name:    "serve",
			Aliases: nil,
			Usage:   "run language server",
			Action:  clicmds.RunServe,
			Flags:   clicmds.ServeFlags(),
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
