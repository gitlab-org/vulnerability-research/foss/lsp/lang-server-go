# lang-server-go

A helping hand for creating and for prototyping language servers in Go. This Go
module assembles the most commonly used functionality for implementing
language servers in Go and provides simple abstractions around the 
[Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/).

This generic language server modules exposes the `FeedbackProvider` interface
that can be implemented by the importing module.

The interface includes the following Methods:

``` go
// ProvideDiagnosticsForFile is a callback to pass diagnostics to the
// language server. The results will be highlighted in the IDE.
ProvideDiagnosticsForFile(file string, action DocumentAction) ([]*CodeDiagnostic, error)
```

``` go
// ProvideDiagnosticsForContent is identical to
// ProvideDiagnosticsForFile with the only difference that it runs on
// the content and captures in-memory change.
ProvideDiagnosticsForContent(filePath, content string, action DocumentAction) ([]*CodeDiagnostic, error)
```

``` go
// ProvideCodeSuggesions provides suggesions based on the current cursor
// position and presented them in the IDE.
ProvideCodeSuggesions(filePath, context string) ([]*CodeSuggesion, error)
```
