package clicmds_test

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lsp/lang-server-go/clicmds"
)

func TestRunGitlabTokenRegister(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	mockStdin(t, testPAT+"\n")

	require.NoError(t, clicmds.RunGitlabTokenRegister(nil))

	tokenFile := filepath.Join(cfgDir, "gitlab-ls", "token")

	require.FileExists(t, tokenFile)

	contents, err := os.ReadFile(tokenFile)
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func mockStdin(t *testing.T, data string) {
	t.Helper()

	tmpStdin, err := os.CreateTemp("", "gitlab-ls-test-stdin")
	require.NoError(t, err)

	_, err = tmpStdin.WriteString(data)
	require.NoError(t, err)

	_, err = tmpStdin.Seek(0, 0)
	require.NoError(t, err)

	origStdin := os.Stdin

	t.Cleanup(func() {
		os.Stdin = origStdin
		os.Remove(tmpStdin.Name())
	})

	os.Stdin = tmpStdin
}
