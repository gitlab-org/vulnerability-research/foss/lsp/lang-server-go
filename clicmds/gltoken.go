package clicmds

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
)

func RunGitlabTokenRegister(_ *cli.Context) error {
	reader := bufio.NewReader(os.Stdin)

	fmt.Printf("Enter GitLab access token: ")

	token, err := reader.ReadString('\n')
	if err != nil {
		return fmt.Errorf("reading GitLab access token from stdin: %w", err)
	}

	token = strings.TrimSpace(token)

	if err := WriteGitlabAccessToken(string(token)); err != nil {
		return err
	}

	fmt.Printf("✔ wrote GitLab access token to %s\n", gitlabAccessTokenFile())

	return nil
}
