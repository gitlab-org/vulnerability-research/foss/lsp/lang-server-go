package clicmds

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/urfave/cli/v2"
)

const (
	LangSrvNameParamName  = "name"
	LangSrvNameEnvVarName = "LANGSRV_NAME"

	LangSrvHostParamName  = "host"
	LangSrvHostEnvVarName = "LANGSRV_HOST"

	LangSrvSrcDirParamName  = "srcdir"
	LangSrvSrcDirEnvVarName = "LANGSRV_SRC_DIR"

	LangSrvGitLabApiTokenParamName  = "gitlab-api-token"
	LangSrvGitLabApiTokenEnvVarName = "LANGSRV_GITLAB_API_TOKEN"

	LangSrvPortParamName  = "port"
	LangSrvPortEnvVarName = "LANGSRV_PORT"

	accessTokenDirPerms  = 0o755
	accessTokenFilePerms = 0o600
)

var gitlabAccessTokenRegexp = regexp.MustCompile(`^glpat-[0-9a-zA-Z\-\_]{20}$`)

// ErrInvalidAccessToken is returned by [WriteAccessToken] if token does not
// look like a valid GitLab access token.
var ErrInvalidAccessToken = errors.New("invalid GitLab access token")

// commonly used options/parameters/flags
// TODO: we probably have to parameterize the env variables in case we want to
// run multiple language servers
var nameParam = &cli.StringFlag{
	Name:    LangSrvNameParamName,
	Usage:   "source dir that contains the source code to run the analysis on.",
	Value:   "",
	EnvVars: []string{LangSrvNameEnvVarName},
}

var sourceDirParam = &cli.StringFlag{
	Name:    LangSrvSrcDirParamName,
	Usage:   "source dir that contains the source code to run the analysis on.",
	Value:   "",
	EnvVars: []string{LangSrvSrcDirEnvVarName},
}

var hostParam = &cli.StringFlag{
	Name:    LangSrvHostParamName,
	Usage:   "host through which the LSP server is reachable.",
	Value:   "",
	EnvVars: []string{LangSrvHostEnvVarName},
}

var gitlabApiTokenParam = &cli.StringFlag{
	Name:     LangSrvGitLabApiTokenParamName,
	Usage:    "GitLab API token",
	Value:    "",
	EnvVars:  []string{LangSrvGitLabApiTokenEnvVarName},
	FilePath: gitlabAccessTokenFile(),
}

var portParam = &cli.IntFlag{
	Name:    LangSrvPortParamName,
	Usage:   "port through which the LSP server is reachable.",
	Value:   6789,
	EnvVars: []string{LangSrvPortEnvVarName},
}

// WriteGitlabAccessToken to a file inside current user's configuration directory.
//
// If the `LANGSRV_GITLAB_API_TOKEN_FILE` environment variable it set, it will
// use that as file path for the access token file.
func WriteGitlabAccessToken(token string) error {
	if !gitlabAccessTokenRegexp.MatchString(token) {
		return ErrInvalidAccessToken
	}

	filename := gitlabAccessTokenFile()
	dir := filepath.Dir(filename)

	if err := os.MkdirAll(dir, accessTokenDirPerms); err != nil {
		return fmt.Errorf("creating directory for access token file %s: %w", dir, err)
	}

	if err := os.WriteFile(filename, []byte(token), accessTokenFilePerms); err != nil {
		return fmt.Errorf("writing access token to %s: %w", filename, err)
	}

	return nil
}

func gitlabAccessTokenFile() string {
	if filename := os.Getenv("LANGSRV_GITLAB_API_TOKEN_FILE"); filename != "" {
		return filename
	}

	cfgDir, err := os.UserConfigDir()
	if err != nil {
		// Error from UserConfigDir function is very rare, so we panic if we
		// encounter this to simplify the usage of this function the majority of
		// the time.
		panic(fmt.Errorf("determining user configuration directory: %w", err))
	}

	return filepath.Join(cfgDir, "gitlab-ls", "token")
}
