package clicmds_test

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/vulnerability-research/foss/lsp/lang-server-go/clicmds"
)

const testPAT = "glpat-deadbeefdeadbeefdead"

func TestWriteAccessToken(t *testing.T) {
	cfgDir := mockUserConfigDir(t)

	require.NoError(t, clicmds.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(filepath.Join(cfgDir, "gitlab-ls", "token"))
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func TestWriteAccessToken_FileExists(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	tokenFile := filepath.Join(cfgDir, "gitlab-ls", "token")
	require.NoError(t, os.MkdirAll(filepath.Dir(tokenFile), 0o755))
	require.NoError(t, os.WriteFile(tokenFile, []byte("glpat-c0ffeec0ffeec0ffeec0ff"), 0o600))

	require.NoError(t, clicmds.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(filepath.Join(cfgDir, "gitlab-ls", "token"))
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))
}

func TestWriteAccessToken_Invalid(t *testing.T) {
	cfgDir := mockUserConfigDir(t)

	require.ErrorIs(t, clicmds.WriteGitlabAccessToken("lolwut?"), clicmds.ErrInvalidAccessToken)

	require.NoFileExists(t, filepath.Join(cfgDir, "gitlab-ls", "token"))
}

func TestWriteAccessToken_TokenFileLocationFromEnv(t *testing.T) {
	cfgDir := mockUserConfigDir(t)
	envTokenFile := filepath.Join(cfgDir, "my-custom-gitlab-token-file")

	t.Setenv("LANGSRV_GITLAB_API_TOKEN_FILE", envTokenFile)

	require.NoError(t, clicmds.WriteGitlabAccessToken(testPAT))

	contents, err := os.ReadFile(envTokenFile)
	require.NoError(t, err)

	require.Equal(t, testPAT, string(contents))

	require.NoFileExists(t, filepath.Join(cfgDir, "gitlab-ls", "token"))
}

// mockUserConfigDir creates a new temporary directory and overrides $HOME and
// $XDG_CONFIG_HOME environment variables for the test with the tempdir
// location. This prevents tests from overwriting the actual access token file
// in a user's configuration directory returned by [os.UserConfigDir].
//
// Returns path to mocked user configuration directory.
func mockUserConfigDir(t *testing.T) string {
	t.Helper()

	tmpHome := t.TempDir()

	t.Setenv("HOME", tmpHome)
	t.Setenv("XDG_CONFIG_HOME", tmpHome)

	cfgDir, err := os.UserConfigDir()
	require.NoError(t, err)

	if !strings.HasPrefix(cfgDir, tmpHome) {
		panic(fmt.Errorf("os.UserConfigDir did not return path within mock home directory %s: %s", tmpHome, cfgDir))
	}

	require.NoError(t, os.MkdirAll(cfgDir, 0755))

	return cfgDir
}
