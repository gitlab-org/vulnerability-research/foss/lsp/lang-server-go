package clicmds

import (
	"fmt"

	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/tliron/glsp"
	protocol "github.com/tliron/glsp/protocol_3_16"
	"github.com/tliron/glsp/server"
	"github.com/tliron/kutil/logging"
	"github.com/urfave/cli/v2"

	// Must include a backend implementation. See kutil's logging/ for other options.
	"github.com/dgraph-io/ristretto"
	_ "github.com/tliron/kutil/logging/simple"
)

var version string = "0.4.0"

var handler protocol.Handler

func ServeFlags() []cli.Flag {
	return []cli.Flag{
		nameParam,
		sourceDirParam,
		gitlabApiTokenParam,
		portParam,
		hostParam,
	}
}

func (severity Severity) ToLspSeverity() protocol.DiagnosticSeverity {
	switch severity {
	case SeverityHigh, SeverityMedium:
		return protocol.DiagnosticSeverityError
	default:
		return protocol.DiagnosticSeverityHint
	}
}

func (feedback *CodeDiagnostic) ToLspDiagnostic() protocol.Diagnostic {
	severity := feedback.Severity.ToLspSeverity()
	diagnostic := protocol.Diagnostic{
		Range:    feedback.Coordinate.toProtocolRange(),
		Severity: &severity,
		Source:   stringPtr(feedback.Origin),
		Message:  feedback.Message,
	}
	return diagnostic
}

func (feedback *CodeDiagnostic) ToCodeAction() protocol.CodeAction {
	diagnostic := feedback.ToLspDiagnostic()
	insertRange := protocol.Range{
		Start: diagnostic.Range.Start,
		End:   diagnostic.Range.Start,
	}

	return protocol.CodeAction{
		Kind:  stringPtr(protocol.CodeActionKindQuickFix),
		Title: feedback.Fix.Hint,
		Edit: &protocol.WorkspaceEdit{
			Changes: map[protocol.DocumentUri][]protocol.TextEdit{
				protocol.DocumentUri(feedback.File): {
					{
						NewText: "",
						Range:   diagnostic.Range,
					},
					{
						NewText: feedback.Fix.Replacement,
						Range:   insertRange,
					},
				},
			},
		},
		Diagnostics: []protocol.Diagnostic{diagnostic},
	}
}

func (c Coordinate) toProtocolRange() protocol.Range {
	return protocol.Range{
		Start: protocol.Position{
			Line:      uint32(c.StartLine),
			Character: uint32(c.StartCol),
		},
		End: protocol.Position{
			Line:      uint32(c.EndLine),
			Character: uint32(c.EndCol),
		},
	}
}

type LspServer struct {
	Configuration    *LspServerConfiguration
	Actions          []protocol.CodeAction
	FeedbackProvider map[string]FeedBackProvider
	CursorContext    *CursorContext
	DocumentCache    *ristretto.Cache
}

func (s *LspServer) CodeActionReset() {
	s.Actions = []protocol.CodeAction{}
}

func checkConfiguration(cfg *LspServerConfiguration) error {
	if cfg.Name == "" {
		return fmt.Errorf("no name specified")
	}

	if cfg.SrcDir == "" {
		return fmt.Errorf("no source dir specified")
	}

	if _, err := os.Stat(cfg.SrcDir); err != nil {
		return err
	}
	return nil
}

func (s *LspServer) Start() error {

	err := checkConfiguration(s.Configuration)
	if err != nil {
		return err
	}

	handler = protocol.Handler{}

	server := server.NewServer(&handler, s.Configuration.Name, false)

	handler.Initialize = func(context *glsp.Context, params *protocol.InitializeParams) (any, error) {
		capabilities := handler.CreateServerCapabilities()

		value := protocol.TextDocumentSyncKindFull
		capabilities.TextDocumentSync.(*protocol.TextDocumentSyncOptions).Change = &value

		// register cmd command
		capabilities.CodeActionProvider = &protocol.CodeActionOptions{
			ResolveProvider: boolPtr(true),
			CodeActionKinds: []protocol.CodeActionKind{
				protocol.CodeActionKindQuickFix,
			},
		}

		capabilities.CompletionProvider = &protocol.CompletionOptions{
			ResolveProvider: boolPtr(true),
		}

		return protocol.InitializeResult{
			Capabilities: capabilities,
			ServerInfo: &protocol.InitializeResultServerInfo{
				Name:    s.Configuration.Name,
				Version: &version,
			},
		}, nil
	}

	handler.Initialized = func(context *glsp.Context, params *protocol.InitializedParams) error {
		return nil
	}

	handler.Shutdown = func(context *glsp.Context) error {
		protocol.SetTraceValue(protocol.TraceValueOff)
		return nil
	}
	handler.SetTrace = func(context *glsp.Context, params *protocol.SetTraceParams) error {
		protocol.SetTraceValue(params.Value)
		return nil
	}

	refreshDiagnosticsOfDocumentOnChange := func(t protocol.DocumentUri, content string, notify glsp.NotifyFunc, delay bool, action DocumentAction) {
		// cleanup all code actions
		s.CodeActionReset()

		feedback := []*CodeDiagnostic{}
		diagnostics := []protocol.Diagnostic{}

		filePath := strings.TrimPrefix(t, "file://")

		for _, provider := range s.FeedbackProvider {
			if content != "" {
				contentFeedback, err := provider.ProvideDiagnosticsForContent(filePath, content, action)
				if err != nil {
					log.Error(err)
					continue
				}
				feedback = append(feedback, contentFeedback...)
			}
		}

		diagnostics = []protocol.Diagnostic{}
		for _, feedback := range feedback {
			diagnostic := feedback.ToLspDiagnostic()
			diagnostics = append(diagnostics, diagnostic)
			if feedback.Fix != nil {
				action := feedback.ToCodeAction()
				s.Actions = append(s.Actions, action)
			} else {
				log.Info("No fix provided")
			}
		}

		go notify(protocol.ServerTextDocumentPublishDiagnostics, protocol.PublishDiagnosticsParams{
			URI:         t,
			Diagnostics: diagnostics,
		})

	}
	refreshDiagnosticsOfDocumentOnSave := func(t protocol.DocumentUri, content string, notify glsp.NotifyFunc, delay bool, action DocumentAction) {
		// cleanup all code actions
		s.CodeActionReset()

		feedback := []*CodeDiagnostic{}
		diagnostics := []protocol.Diagnostic{}

		filePath := strings.TrimPrefix(t, "file://")
		for _, provider := range s.FeedbackProvider {
			fileFeedback, err := provider.ProvideDiagnosticsForFile(filePath, action)
			if err != nil {
				log.Error(err)
				continue
			}

			feedback = append(feedback, fileFeedback...)

		}

		diagnostics = []protocol.Diagnostic{}
		for _, feedback := range feedback {
			diagnostic := feedback.ToLspDiagnostic()
			diagnostics = append(diagnostics, diagnostic)
			if feedback.Fix != nil {
				action := feedback.ToCodeAction()
				s.Actions = append(s.Actions, action)
			} else {
				log.Info("No fix provided")
			}
		}

		go notify(protocol.ServerTextDocumentPublishDiagnostics, protocol.PublishDiagnosticsParams{
			URI:         t,
			Diagnostics: diagnostics,
		})
	}

	handler.TextDocumentCompletion = func(context *glsp.Context, params *protocol.CompletionParams) (interface{}, error) {
		s.UpdateCursorContext(params.TextDocument.URI, params.Position)
		if s.CursorContext.Empty() {
			log.Info("context empty")
			return nil, nil
		}

		allSuggestions := []*CodeSuggesion{}
		for _, provider := range s.FeedbackProvider {
			suggesions, err := provider.ProvideCodeSuggesions(strings.TrimPrefix(params.TextDocument.URI, "file://"), s.CursorContext)
			if err != nil {
				log.Error(err)
				return nil, err
			}
			allSuggestions = append(allSuggestions, suggesions...)
		}

		ret := []protocol.CompletionItem{}
		for _, suggestion := range allSuggestions {
			ret = append(ret, suggestion.ToCompletionItem())
		}

		return ret, nil
	}

	handler.TextDocumentDidOpen = func(context *glsp.Context, params *protocol.DidOpenTextDocumentParams) error {
		refreshDiagnosticsOfDocumentOnSave(params.TextDocument.URI, "", context.Notify, false, DocumentOpen)
		return nil
	}

	handler.TextDocumentDidChange = func(context *glsp.Context, params *protocol.DidChangeTextDocumentParams) error {
		if len(params.ContentChanges) == 0 {
			return nil
		}
		lastChange := params.ContentChanges[len(params.ContentChanges)-1].(protocol.TextDocumentContentChangeEventWhole)
		refreshDiagnosticsOfDocumentOnChange(params.TextDocument.URI, lastChange.Text, context.Notify, false, DocumentChange)
		// dirty hack

		log.Info("updating document cache")
		s.DocumentCache.Set(params.TextDocument.URI, lastChange.Text, 1)
		return nil
	}

	handler.TextDocumentDidSave = func(context *glsp.Context, params *protocol.DidSaveTextDocumentParams) error {
		refreshDiagnosticsOfDocumentOnSave(params.TextDocument.URI, "", context.Notify, false, DocumentSave)
		s.DocumentCache.Del(params.TextDocument.URI)
		return nil
	}

	handler.TextDocumentDidClose = func(context *glsp.Context, params *protocol.DidCloseTextDocumentParams) error {
		s.DocumentCache.Del(params.TextDocument.URI)
		return nil
	}

	handler.TextDocumentCodeAction = func(context *glsp.Context, params *protocol.CodeActionParams) (interface{}, error) {
		return s.Actions, nil
	}

	handler.TextDocumentHover = func(context *glsp.Context, params *protocol.HoverParams) (*protocol.Hover, error) {
		log.Debug("Updating cursor context")
		s.UpdateCursorContext(params.TextDocument.URI, params.Position)
		return nil, nil
	}

	// This increases logging verbosity (optional)
	logging.Configure(1, nil)
	if s.Configuration.Stdio {
		return server.RunStdio()
	}
	return server.RunTCP(s.Configuration.ServerUrl())
}

type LspServerConfiguration struct {
	Stdio                                                 bool
	SrcDir, Token, Name, Url                              string
	Port, CursorWindowWidthBefore, CursorWindowWidthAfter uint
}

func NewLspServerConfiguration(name, srcDir, url, token string, cursorBefore, cursorAfter, port uint) (*LspServerConfiguration, error) {
	cfg := &LspServerConfiguration{
		SrcDir:                  srcDir,
		Stdio:                   len(url) == 0,
		Name:                    name,
		Token:                   token,
		Url:                     url,
		Port:                    port,
		CursorWindowWidthBefore: cursorBefore,
		CursorWindowWidthAfter:  cursorAfter,
	}
	return cfg, checkConfiguration(cfg)
}

func (cfg *LspServerConfiguration) ServerUrl() string {
	return fmt.Sprintf("localhost:%d", cfg.Port)
}

func (cfg *LspServerConfiguration) String() string {
	return fmt.Sprintf("Configuration: \nname: %s\nsrcdir: %s\nurl: %s\nstdio: %t\nport %d", cfg.Name, cfg.SrcDir, cfg.Url, cfg.Stdio, cfg.Port)
}

func NewLspServer(cfg *LspServerConfiguration) *LspServer {
	l := &LspServer{
		Configuration:    cfg,
		FeedbackProvider: map[string]FeedBackProvider{},
		Actions:          []protocol.CodeAction{},
		CursorContext:    &CursorContext{},
	}

	var err error
	l.DocumentCache, err = ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,     // number of keys to track frequency of (10M).
		MaxCost:     1 << 30, // maximum cost of cache (1GB).
		BufferItems: 64,      // number of keys per Get buffer.
	})

	if err != nil {
		panic(err)
	}
	return l
}

func (l *LspServer) AddFeedbackProvider(name string, provider FeedBackProvider) {
	l.FeedbackProvider[name] = provider
}

func (l *LspServer) UpdateCursorContext(doc protocol.DocumentUri, pos protocol.Position) error {
	log.Info("update context")
	var content string
	hitContent, hit := l.DocumentCache.Get(doc)
	if !hit {
		path := strings.TrimPrefix(doc, "file://")
		byteContent, err := os.ReadFile(path)
		if err != nil {
			return err
		}
		content = string(byteContent)
	} else {
		content = hitContent.(string)
	}

	idx := pos.IndexIn(content)

	contextBefore := content[Max(0, idx-int(l.Configuration.CursorWindowWidthBefore)):idx]
	contextAfter := content[idx:Min(len(content), idx+int(l.Configuration.CursorWindowWidthAfter))]

	l.CursorContext.Before = contextBefore
	l.CursorContext.After = contextAfter

	log.Debug(l.CursorContext.String())
	return nil
}

func Min[T protocol.UInteger | int](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func Max[T protocol.UInteger | int](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func Overlap(rangea, rangeb protocol.Range) bool {
	var small, big protocol.Range
	if rangea.Start.Line < rangeb.Start.Line {
		small = rangea
		big = rangeb
	} else {
		small = rangeb
		big = rangea
	}

	if small.End.Line < big.Start.Line {
		return false
	}
	return true
}

func RunServe(cli *cli.Context) error {
	log.Infof("Start LSP server")

	srcdir := cli.String(LangSrvSrcDirParamName)
	name := cli.String(LangSrvNameParamName)
	url := cli.String(LangSrvHostParamName)
	token := cli.String(LangSrvGitLabApiTokenParamName)
	port := cli.Uint(LangSrvPortParamName)

	config, err := NewLspServerConfiguration(name, srcdir, url, token, 300, 300, port)
	if err != nil {
		return err
	}

	log.Info(config.String())

	server := NewLspServer(config)

	return server.Start()
}

func stringPtr(v string) *string {
	s := v
	return &s
}

func boolPtr(v bool) *bool {
	s := v
	return &s
}

func intPtr(v int) *int {
	b := v
	return &b
}

func isRangeEmpty(pos protocol.Range) bool {
	return pos.Start == pos.End
}
